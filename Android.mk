# Useful adb line options:
# adb shell setprop log.redirect-stdio true
# adb shell setprop dalvik.vm.jniopts logThirdPartyJni

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE            := pie_arm
LOCAL_MODULE_TAGS       := optional
LOCAL_SHARED_LIBRARIES  := libdl
LOCAL_ARM_MODE          := arm

LOCAL_SRC_FILES         := pie-arm-decoder.c pie-arm-encoder.c pie-arm-field-decoder.c

LOCAL_CFLAGS            += -Wall -march=armv7-a -mcpu=cortex-a9 -mtune=cortex-a9 -mfpu=neon -Os
LOCAL_CFLAGS            += -g -std=gnu99

LOCAL_LDFLAGS           := -Wl,--exclude-libs=libgcc.a,-z,execstack
#LOCAL_LDFLAGS           += -static -ldl -Wl

# explicitly out implicit libs like libdl and libc
LOCAL_SYSTEM_SHARED_LIBRARIES :=
LOCAL_STATIC_LIBRARIES += libc
include $(BUILD_STATIC_LIBRARY)


include $(CLEAR_VARS)
LOCAL_MODULE            := pie_thumb
LOCAL_MODULE_TAGS       := optional
LOCAL_SHARED_LIBRARIES  := libdl
LOCAL_ARM_MODE          := arm

LOCAL_SRC_FILES         := pie-thumb-decoder.c pie-thumb-encoder.c pie-thumb-field-decoder.c

LOCAL_CFLAGS            += -Wall -march=armv7-a -mcpu=cortex-a9 -mtune=cortex-a9 -mfpu=neon -Os
LOCAL_CFLAGS            += -g -std=gnu99

LOCAL_LDFLAGS           := -Wl,--exclude-libs=libgcc.a,-z,execstack
#LOCAL_LDFLAGS           += -static -ldl -Wl

# explicitly out implicit libs like libdl and libc
LOCAL_SYSTEM_SHARED_LIBRARIES :=
LOCAL_STATIC_LIBRARIES += libc
include $(BUILD_STATIC_LIBRARY)
